<?php

require_once 'Dog.php';

class Poodle extends Dog {
    public $type;
    public function set_type($height) {
        if ($height<10) $this->type = 'Toy';
        elseif ($height>15) $this->type = 'Standard';
        else $this->type = 'Miniature';
    }

    public function bark() {
        echo $this->name.' poodle says au au!';
    }
}