<?php
require_once 'HardDrive.php';

final class SSDHardDrive extends HardDrive
{
    protected $speed;
    

    /**
     * Set the value of speed
     *
     * @return  self
     */ 
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    public function getCard()
    {
        return 'SSD card';
    }

    public function __toString()
    {
        return 'SSD drive';
    }
}