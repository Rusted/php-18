<?php

require_once 'Device.php';
require_once 'ProductInterface.php';


final class Memory extends Device implements ProductInterface
{
    protected $price;

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getName(): string
    {
        return 'memory';
    }
}