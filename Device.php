<?php
abstract class Device
{
    protected $serialNumber;
    protected $manufacturer;
    protected $model;
    protected $sku;

    public function __construct($serial, $sku)
    {
        $this->serialNumber = $serial;
        $this->sku = $sku;
    }

    /**
     * Set the value of manufacturer
     *
     * @return  self
     */ 
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Set the value of model
     *
     * @return  self
     */ 
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    abstract public function getInventoryDetails(): string;
}