<?php

require_once 'ProductInterface.php';
class Cart
{
    protected $products = [];
    protected $totalPrice = 0;

    public function addProduct(ProductInterface $product)
    {
        $this->products[] = $product;
        $this->totalPrice += $product->getPrice(); 
    }
}