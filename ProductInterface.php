<?php

interface ProductInterface
{
    public function setPrice($price);

    public function getPrice();

    public function getName(): string;
}